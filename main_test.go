package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/spf13/afero"
)

var fs afero.Fs = afero.NewOsFs() 

func TestMain(m *testing.M) {
    code := m.Run()
    os.Exit(code)
}


func TestShowData(t *testing.T) {
    fileName := "test.csv"
    sampleData := "Project Name,Token,Permission,User ID,User Name,Expiry Date\nProject Beta,token456,\"read,write\",102,Bob,2024/6/30\n"
    afero.WriteFile(fs, fileName, []byte(sampleData), 0644)
    defer fs.Remove(fileName)

    var buf bytes.Buffer
    oldStdout := os.Stdout
    r, w, _ := os.Pipe()
    os.Stdout = w

    showData(fileName) 

    w.Close()
    os.Stdout = oldStdout
    io.Copy(&buf, r)
    
    expected := "Project Name | Token | Permission | User ID | User Name | Expiry Date\nProject Beta | token456 | read,write | 102 | Bob | 2024/6/30\n"
    output := buf.String()

    if output != expected {
        t.Errorf("Expected output %q, got %q", expected, output)
    }
    fmt.Println("ShowData output:", output) 
}

func TestAddData(t *testing.T) {
    fileName := "test_add.csv"
    file, err := fs.Create(fileName)
    if err != nil {
        t.Fatalf("Failed to create file: %v", err)
    }
    file.Close()
    defer fs.Remove(fileName)

    data := "NewProj,123ABC,Read,1001,JohnDoe,2024-12-31"
    addData(fileName, data)

    contents, err := afero.ReadFile(fs, fileName)
    if err != nil {
        t.Fatalf("Failed to read file: %v", err)
    }
    expected := "NewProj,123ABC,Read,1001,JohnDoe,2024-12-31\n"
    if string(contents) != expected {
        t.Errorf("Expected file content %q, got %q", expected, string(contents))
    }
    t.Logf("AddData contents: %s", string(contents))
}

func TestDeleteData(t *testing.T) {
    fileName := "test_delete.csv"
    initialData := "Project Name,Token,Permission,User ID,User Name,Expiry Date\n" +
                   "Project Alpha,token123,read,001,Alice,2024/1/1\n" +
                   "Project Beta,token456,write,002,Bob,2024/6/30\n"
    afero.WriteFile(fs, fileName, []byte(initialData), 0644)
    defer fs.Remove(fileName)

    
    deleteData(fileName, 1) 

    finalData, err := afero.ReadFile(fs, fileName)
    if err != nil {
        t.Fatalf("Failed to read file after delete: %v", err)
    }

    
    expectedData := "Project Name,Token,Permission,User ID,User Name,Expiry Date\n" +
                    "Project Beta,token456,write,002,Bob,2024/6/30\n" 
    if string(finalData) != expectedData {
        t.Errorf("Expected data %q, got %q", expectedData, string(finalData))
    }
}

func TestUpdateData(t *testing.T) {
    fileName := "test_update.csv"
    initialData := "Project Name,Token,Permission,User ID,User Name,Expiry Date\nProject Beta,t4446,\"read,322\",102,Bob,2024/6/30\nProject Beta,token456,write,102,Bob,2024/6/30\nProject Beta,token456,\"read,write\",102,mean,2024/3/30\n"
    afero.WriteFile(fs, fileName, []byte(initialData), 0644)
    defer fs.Remove(fileName)

    newData := "Project Beta,token456,\"read,write\",102,Bob,2024/6/30"
    updateData(fileName, 1, newData)

    updatedContents, err := afero.ReadFile(fs, fileName)
    if err != nil {
        t.Fatalf("Failed to read file after update: %v", err)
    }
    expected := "Project Name,Token,Permission,User ID,User Name,Expiry Date\nProject Beta,token456,\"read,write\",102,Bob,2024/6/30\nProject Beta,token456,write,102,Bob,2024/6/30\nProject Beta,token456,\"read,write\",102,mean,2024/3/30\n"
    if string(updatedContents) != expected {
        t.Errorf("Expected file content %q, got %q", expected, string(updatedContents))
    }
    t.Logf("UpdateData final contents: %s", string(updatedContents)) 
}
