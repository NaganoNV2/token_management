package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {
    var action, filePath, data string
    var lineNum int
    flag.StringVar(&action, "action", "show", "Action to perform (show/add/delete/update)")
    flag.StringVar(&filePath, "file", "", "Path to the CSV file")
    flag.StringVar(&data, "data", "", "Data for add/update in CSV format")
    flag.IntVar(&lineNum, "line", -1, "Line number for delete/update")
    flag.Parse()

    switch action {
    case "show":
        showData(filePath)
    case "add":
        addData(filePath, data)
    case "delete":
        deleteData(filePath, lineNum)
    case "update":
        updateData(filePath, lineNum, data)
    default:
        fmt.Println("Unsupported action. Use 'show', 'add', 'delete', or 'update'.")
    }
}

func showData(filePath string) {
    file, err := os.Open(filePath)
    if err != nil {
        fmt.Println("Error opening file:", err)
        return
    }
    defer file.Close()

    reader := csv.NewReader(file)
    records, err := reader.ReadAll()
    if err != nil {
        fmt.Println("Error reading CSV data:", err)
        return
    }

    for _, record := range records {
        fmt.Println(strings.Join(record, " | "))
    }
}

func addData(filePath, data string) {
    r := csv.NewReader(strings.NewReader(data))
    record, err := r.Read()
    if err != nil {
        fmt.Println("Error reading CSV data:", err)
        return
    }

    if len(record) != 6 {
        fmt.Println("Error: Data must contain exactly 6 fields (ProjectName, Token, Permission, UserID, UserName, ExpiryDate)")
        return
    }

    file, err := os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY, 0644)
    if err != nil {
        fmt.Println("Error opening file:", err)
        return
    }
    defer file.Close()

    writer := csv.NewWriter(file)
    defer writer.Flush()

    err = writer.Write(record)
    if err != nil {
        fmt.Println("Error writing to CSV file:", err)
        return
    }

    fmt.Println("Data added successfully.")
}

func deleteData(filePath string, lineNum int) {
	records, err := readCSV(filePath)
	if err != nil {
		fmt.Println("Error reading CSV data:", err)
		return
	}

	if lineNum < 0 || lineNum >= len(records) {
		fmt.Println("Error: Line number out of range.")
		return
	}

	
	records = append(records[:lineNum], records[lineNum+1:]...)

	writeCSV(filePath, records)
	fmt.Println("Line deleted successfully.")
}

func updateData(filePath string, lineNum int, data string) {
	records, err := readCSV(filePath)
	if err != nil {
		fmt.Println("Error reading CSV data:", err)
		return
	}

	if lineNum < 0 || lineNum >= len(records) {
		fmt.Println("Error: Line number out of range.")
		return
	}

	r := csv.NewReader(strings.NewReader(data))
	record, err := r.Read()
	if err != nil {
		fmt.Println("Error parsing CSV data:", err)
		return
	}

	if len(record) != 6 {
		fmt.Println("Error: Data must contain exactly 6 fields (ProjectName, Token, Permission, UserID, UserName, ExpiryDate)")
		return
	}

	// 指定された行を更新
	records[lineNum] = record

	writeCSV(filePath, records)
	fmt.Println("Line updated successfully.")
}

func readCSV(filePath string) ([][]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	return records, nil
}

func writeCSV(filePath string, records [][]string) {
	file, err := os.Create(filePath)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	err = writer.WriteAll(records)
	if err != nil {
		fmt.Println("Error writing to CSV file:", err)
		return
	}
}
