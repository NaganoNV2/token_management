# パーソナルアクセストークン管理 CLI ツール

このツールは、パーソナルアクセストークンを管理するための簡単なコマンドラインインターフェース（CLI）アプリケーションです。CSV ファイルを使用してアクセストークンを追加、表示する機能を提供します。

## 機能

- **表示**: CSV ファイルに保存されているアクセストークンの一覧を表示します。
- **追加**: 新しいアクセストークンを CSV ファイルに追加します。

## 使用方法

このツールを使用するには、以下のコマンドラインオプションを利用できます。

- `-action`: 実行するアクション（`show`、`add`、`delete`、`update`）。`show`を指定するとトークンの一覧が表示され、`add`を指定すると新しいトークンが追加され、`delete`で指定された行を削除し、`update`で指定された行のデータを更新します。
- `-file`: CSV ファイルのパスを選択します。
- -`data`: CSV フォーマットで追加/更新するデータ。アクションが`add`または`update`の場合にのみ必要です。
- `-line`: 削除または更新する行の番号。アクションが`delete`または`update`の場合にのみ必要です。
-

### トークンの表示

```
go run main.go -action show -file your_file_path.csv
```

### トークンの追加

```
go run main.go -action add -file "your_file_path.csv" -data "ProjectName,Token,Permission,UserID,UserName,ExpiryDate"TokenSecret,ExpirationDate"
```

```
go run main.go -action add -file "D:\webto\access_token_management\projects_tokens.csv" -data "Project Test,tokenXYZ,\"read,write\",105,Me,2025/12/31"
```

## 行の削除(例)

```
go run main.go -action delete -file "D:\webto\access_token_management\projects_tokens.csv" -line 4
```

## 行の更新(例)

```
go run main.go -action update -file "D:\webto\access_token_management\projects_tokens.csv" -line 1 -data "Project Beta,token456,\"read,write\",102,Bob,2024/6/30"
```

#### ※windows コマンドプロンプトのみ確認済み

### インストール方法

- このツールは Go 言語を使用しています
- ソースコードをダウンロード後、以下のコマンドで実行可能ファイルをビルドできます。

```
go build main.go
```

### 注意事項

- このツールは、CSV ファイルの形式が正しいことを前提としています。不正な形式のファイルを指定した場合、エラーが発生する可能性があります。
- セキュリティ上の理由から、アクセストークンを含むファイルは安全な場所に保存してください。

### 注意事項

# テスト

このコードは Go 言語で書かれたテストスイートです。主に afero というファイルシステム抽象化ライブラリを使用して、ファイル操作のテストを容易に行っています。afero.Fs は抽象化されたファイルシステムを提供し、テスト中は実際のファイルシステムの代わりに使用されることがよくあります。以下は各関数の具体的な説明です：

## TestMain

TestMain 関数はテストのセットアップとクリーンアップを行うために使われます。ここでは、全てのテストが実行された後にプログラムを終了するために os.Exit(code)が呼ばれています。

## TestShowData

このテストは、CSV 形式のサンプルデータをファイルに書き込み、その後 showData 関数を呼び出して出力を検証します。出力は標準出力にリダイレクトされ、期待されるフォーマット（各フィールドが | で区切られている）であることを確認します。

## TestAddData

TestAddData は、新しいデータを CSV ファイルに追加する処理をテストします。指定されたデータがファイルに正しく追加されたかどうかを検証し、ファイルの内容が期待通りであるかを確認します。

## TestDeleteData

この関数は、指定された行を CSV ファイルから削除する機能をテストします。削除後のファイルの内容が期待したものになっているかを検証し、正しい行が削除されたかを確認します。

## TestUpdateData

TestUpdateData は、CSV ファイル内の特定の行を新しいデータで更新する処理をテストします。更新後のファイルの内容が期待通りかを検証し、正確な更新が行われたことを確認します。

####　実行コマンド

```
go test
```
