import requests

# GitLabの設定
ACCESS_TOKEN = 'XXXXXX'
GITLAB_API_URL = 'https://gitlab.com/api/v4'
GROUP_ID = 'XXXXXX'  # dev-krcのグループID

# ヘッダーの設定
headers = {'Authorization': f'Bearer {ACCESS_TOKEN}'}

# グループ内のプロジェクトを取得
response = requests.get(f'{GITLAB_API_URL}/groups/{GROUP_ID}/projects', headers=headers)
projects = response.json()

# 各プロジェクトの環境変数を取得
for project in projects:
    project_id = project['id']
    project_name = project['name']
    env_vars_response = requests.get(f'{GITLAB_API_URL}/projects/{project_id}/variables', headers=headers)
    env_vars = env_vars_response.json()

    print(f'Project: {project_name} (ID: {project_id})')
    for var in env_vars:
        if "key" in var and "TOKEN" in var["key"]:
            print(f' - {var["key"]}: {var["value"]}')

